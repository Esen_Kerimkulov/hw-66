import React, {Component, Fragment} from 'react';
import axios from '../../axios-quotes';
import QuoteForm from "../../components/QuotetForm/QuoteForm";
import ErrorBoundary from "../../components/ErrorBoundary/ErrorBoundary";



class AddQuote extends Component {
    addQuote = quote => {
        axios.post('quotes.json', quote).then(() => {
            this.props.history.replace('/');
        });
    };
    render() {
        return (
            <ErrorBoundary>
            <Fragment>
                <h1>Add new quote</h1>
                <QuoteForm onSubmit={this.addQuote}/>
            </Fragment>
            </ErrorBoundary>
        );
    }
}

export default AddQuote;