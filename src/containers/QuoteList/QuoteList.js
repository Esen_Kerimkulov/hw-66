import React, {Component} from 'react';
import {
    Button,
    Card,
    CardBody,
    CardColumns,
    CardFooter,
    CardTitle,
    Col,
    ListGroup, ListGroupItem,
    NavLink,
    Row
} from "reactstrap";

import {NavLink as RouterNavLink} from "react-router-dom";

import axios from '../../axios-quotes';
import CardText from "reactstrap/es/CardText";

import {CATEGORIES} from "../../constants";
import errorHandler from "../../hoc/withErrorHandler/ErrorHandler";
import Spinner from "../../components/UI/Spinner/Spinner";
import ErrorBoundary from "../../components/ErrorBoundary/ErrorBoundary";


class QuoteList extends Component {
    state = {
      quotes: null
    };

    loadingData() {
        let url = 'quotes.json';

        const categoryId = this.props.match.params.categoryId;
        if (categoryId) {
            url += `?orderBy="category"&equalTo="${categoryId}"`
        }
        axios.get(url).then(response => {
            const quotes = Object.keys(response.data).map(id => {
                return {...response.data[id], id};
            });

            this.setState({quotes});
        });
    }

    componentDidMount() {
        this.loadingData();
        }

    componentDidUpdate(prevProps) {
        if (this.props.match.params.categoryId !== prevProps.match.params.categoryId) {
            this.loadingData();
        }
    }

    render() {
        let quotes = null;

        if(!this.state.quotes) {
            quotes = <Spinner/>
        }

        if (this.state.quotes) {
            quotes = this.state.quotes.map(quote => (
                <Card className="Card" key={quote.id} style={{marginTop: '20px'}}>
                    <CardBody>
                        <CardText>{quote.quote}</CardText>
                        <CardTitle><b>- {quote.author}</b></CardTitle>
                    </CardBody>
                    <CardFooter>
                        <RouterNavLink to={'/quotes/' + quote.id + '/edit'}>
                            <Button color="warning" style={{marginRight: '10px'}}>Edit</Button>
                        </RouterNavLink>
                    </CardFooter>
                </Card>
            ));
        }
        
        return (
            <Row>
                    <ListGroup style={{marginTop: '20px'}}>
                            <ListGroupItem active>Categories:</ListGroupItem>
                        <ListGroupItem >
                            <NavLink tag={RouterNavLink} to="/" exact>All</NavLink>
                        </ListGroupItem>
                        {Object.keys(CATEGORIES).map(categoryId => (
                        <ListGroupItem key={categoryId}>
                            <NavLink
                                tag={RouterNavLink}
                                to={"/quotes/" + categoryId }
                                exact
                            >
                                {CATEGORIES[categoryId]}
                            </NavLink>
                        </ListGroupItem>
                            ))}
                    </ListGroup>
                    <Col sm={10}>
                        <CardColumns>
                        {quotes}
                        </CardColumns>
                    </Col>
            </Row>
        );
    }
}

export default errorHandler(QuoteList, axios);