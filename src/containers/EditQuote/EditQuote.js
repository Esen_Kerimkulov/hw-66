import React, {Component, Fragment} from 'react';
import axios from '../../axios-quotes';
import QuoteForm from "../../components/QuotetForm/QuoteForm";
import Button from "reactstrap/es/Button";
import Spinner from "../../components/UI/Spinner/Spinner";
import errorHandler from "../../hoc/withErrorHandler/ErrorHandler";

class EditQuote extends Component {
    state = {
        quote: null
    };

    getQuoteUrl = () => {
        const id = this.props.match.params.id;
        return 'quotes/' + id + '.jsonb';        // BAG IS HERE.......
    };

    componentDidMount() {
        axios.get(this.getQuoteUrl()).then(response => {
            if (!response.data) return;

            this.setState({quote:response.data})
        })
    }

    editQuote = quote => {
        axios.put(this.getQuoteUrl(), quote).then(() => {
            this.props.history.replace('/');
        });
    };


    deleteQuote = () => {
        const id = this.props.match.params.id;

        axios.delete('quotes/' + id + '.json').then(() => {
            this.props.history.push('/')

        })
    };


    render() {
        let form = <QuoteForm onSubmit={this.editQuote} quote={this.state.quote}/>;

        if (!this.state.quote) {
            form = <Spinner/>
        }
        return (
            <Fragment>
                <h1>Edit quote</h1>
                {form}
                <Button color="danger"
                        onClick={this.deleteQuote}
                        style={{marginLeft: '16.9%',
                                padding: '7px 8px',}}
                >Delete</Button>
            </Fragment>

        );
    }
}

export default errorHandler(EditQuote, axios);