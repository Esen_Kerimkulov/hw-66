import React, {Component} from 'react';

import {CATEGORIES} from "../../constants";

import {Button, Card, Col, Form, FormGroup, Input, Label} from "reactstrap";

class QuoteForm extends Component {
    constructor(props) {
        super(props);

        if (props.quote) {
            this.state = {...props.quote};
        } else {
            this.state = {
                author: '',
                category: Object.keys(CATEGORIES)[0],
                quote: ''
            };
        }
    }

    valueChanged = event => {
        this.setState({[event.target.name]: event.target.value});
    };

    submitHandler = event => {
        event.preventDefault();
        this.props.onSubmit({...this.state});
    };




    render() {
        if (Math.random() > 0.7) throw new Error('Well, this happened!');
        return (
            <Form className="PostForm" onSubmit={this.submitHandler} style={{marginTop: '50px'}}>
                <FormGroup row >
                        <Label for="category" sm={2}>Category</Label>
                        <Col sm={10}>
                            <Input type="select" name="category" id="category"  value={this.state.category} onChange={this.valueChanged}
                            >
                                {Object.keys(CATEGORIES).map(categoryId => (
                                    <option key={categoryId} value={categoryId}>{CATEGORIES[categoryId]}</option>
                                ))};
                            </Input>
                        </Col>
                    </FormGroup>
                        <FormGroup row >
                    <Label for="author" sm={2}>Author</Label>
                    <Col sm={10}>
                        <Input type="text" name="author"
                               value={this.state.author} onChange={this.valueChanged}
                        />
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Label for="quote" sm={2}>Quote Text</Label>
                    <Col sm={10}>
                        <Input type="textarea" name="quote"
                               value={this.state.quote} onChange={this.valueChanged}
                        />
                    </Col>
                </FormGroup>

                <FormGroup row>
                    <Col>
                        <Button style={{marginLeft: '189px '}} color="info" type="submit">Save</Button>
                    </Col>
                </FormGroup>
            </Form>
        );
    }
}

export default QuoteForm;