import React from 'react';

import './Spinner.css';

const Spinner = () => {
    return (
        <div className="loading">
            <div className="loading-bar"></div>
            <div className="loading-bar"></div>
            <div className="loading-bar"></div>
            <div className="loading-bar"></div>
        </div>

    );
};

export default Spinner;