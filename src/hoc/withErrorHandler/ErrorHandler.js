import React, {Component, Fragment} from 'react';
import Modal from "../../components/UI/Modal/Modal";

const errorHandler = (WrappedComponent, axios) => {
    return class ErrorHOC extends Component {

        constructor(props) {
            super(props);

            this.state = {
                error: null
            };

            this.state.interceptorId = axios.interceptors.response.use(res => res, error => {
                this.setState({error});
                throw error;
            });
        }

        componentWillUnmount() {
            axios.interceptors.response.eject(this.state.interceptorId);
        }

        dismissError = () => {
            this.setState({error: null})
        };

        render () {

            return (
                <Fragment>
                    <Modal show={this.state.error} close={this.dismissError}>
                        {this.state.error && this.state.error.message}
                    </Modal>
                <WrappedComponent {...this.props}/>;
                </Fragment>
            )
        }
    };
};

export default errorHandler;