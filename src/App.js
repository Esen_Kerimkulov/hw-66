import React, {Component, Fragment} from 'react';

import {Route, Switch, NavLink as RouterNavLink} from "react-router-dom";
import AddQuote from "./containers/AddQuote/AddQuote";
import {Collapse, Container, Nav, Navbar, NavbarBrand, NavbarToggler, NavItem, NavLink} from "reactstrap";
import QuoteList from "./containers/QuoteList/QuoteList";
import EditQuote from "./containers/EditQuote/EditQuote";



class App extends Component {
  render() {
    return (
        <Fragment>
          <Navbar color="light" light expand="md">
            <NavbarBrand>Quotes Central</NavbarBrand>
            <NavbarToggler />
            <Collapse isOpen navbar>
              <Nav className="ml-auto" navbar>
                <NavItem>
                  <NavLink tag={RouterNavLink} to="/" exact>Quotes</NavLink>
                </NavItem>
                <NavItem>
                  <NavLink  tag={RouterNavLink} to="/add">Submit new quote</NavLink>
                </NavItem>
              </Nav>
            </Collapse>
          </Navbar>
          <Container>
            <Switch>
              <Route path="/" exact component={QuoteList}/>
              <Route path="/add" exact component={AddQuote}/>
              <Route path="/quotes/:id/edit" component={EditQuote}/>
              <Route path="/quotes/:categoryId" component={QuoteList}/>
              <Route render={() => <h1>Not found</h1>}/>
            </Switch>
          </Container>
        </Fragment>
    );
  }
}

export default App;